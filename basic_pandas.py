import pandas as pd

a = ['a', 'b', 'c']

"""Series"""

b = pd.Series(a)
print('\t Series\n',b)

"""Series 1"""

data = {'name' : 'Adam', 'year' : '20', 'role' : 'student'}
data_s = pd.Series(data)
print('\tSeries 1\n', data_s)

"""DataFrame"""

data1 = {"name":pd.Series(['Ivan', 'Olena', 'Oleg'], index=['v1', 'v2', 'v3']),
        "mark": pd.Series([10,5,6], index=['v1','v2','v3'])}


data_f1 = pd.DataFrame(data1)
print('\t DataFrame\n', data_f1)


