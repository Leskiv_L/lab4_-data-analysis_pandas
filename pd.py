import pandas as pd

# 2
df = pd.read_csv("E:\\3 course\\машинне навчання\\лаб4_pandas\\StPer.csv")
print('table\t', df.head())
# d = df.fillna(0)
d = df.dropna()
print('table after validation\n\t', d.head())
d.to_csv('StPer_validation.csv')

# 3 left join, inner join, right join, full outer join.
df1 = pd.read_csv("E:\\3 course\\машинне навчання\\лаб4_pandas\\student.csv")
df2 = pd.read_csv("E:\\3 course\\машинне навчання\\лаб4_pandas\\mark.csv")
print("student table\n", df1)
print("mark table\n", df2)
print('inner join\n', pd.merge(df1, df2, how="inner"))
print('outer join\n', pd.merge(df1, df2, how="outer"))
print('left join\n', pd.merge(df1, df2, how="left"))
print("right join\n", pd.merge(df1, df2, how="right"))

# 4 агрегація (sum(), mean(), median(), min(), max())
print('\t\tsum\n', d.sum())
print('\t\tmean\n', d.mean())
print('\t\tmedian\n', d.median())
print('\t\tmin()\n', d.min())
print('\t\tmax()\n', d.max())

#  4 групування, фільтрація
print('\t\tGROUP BY GENDER\n', d.groupby('gender').mean())
#print('\t\tGROUPBY\n', d.groupby('gender').agg(['mean', 'count']))


def filter_funk(x):
    return x['reading score'].median() > 60


print('\t\tfliter\n', d.groupby('gender').filter(filter_funk))
